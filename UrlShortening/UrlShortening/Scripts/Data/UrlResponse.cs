using Newtonsoft.Json;
using UrlShortening.Scripts.Domain;

namespace UrlShortening.Scripts.Data
{
    public struct UrlResponse
    {
        public string Url;
        public string ShortenedUrl;

        public UrlResponse(UrlData urlData)
        {
            Url = urlData.Url;
            ShortenedUrl = urlData.ShortenedUrl;
        }
        
        public string ToJson() 
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}