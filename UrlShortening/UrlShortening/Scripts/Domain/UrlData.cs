using System;

namespace UrlShortening.Scripts.Domain
{
    public class UrlData
    {
        public string Url;
        public string ShortenedUrl;
        public string Token;
        public DateTime CreatedDate;
        public int Requested;
        public int UrlSubdomains;

        public UrlData(string url, string shortenedUrl, string token, DateTime createdDate, int requested, int urlSubdomains)
        {
            Url = url;
            ShortenedUrl = shortenedUrl;
            Token = token;
            CreatedDate = createdDate;
            Requested = requested;
            UrlSubdomains = urlSubdomains;
        }

        public void OnRequest()
        {
            Requested += 1;
        }
    }
}