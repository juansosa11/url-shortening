namespace UrlShortening.Scripts.Domain.Action
{
    public interface ITokenGenerator
    {
        string Execute();
    }
}