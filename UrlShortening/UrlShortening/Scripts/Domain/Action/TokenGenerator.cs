using System;
using System.Linq;

namespace UrlShortening.Scripts.Domain.Action
{
    public class TokenGenerator : ITokenGenerator
    {
        public string Execute() {
            var token = string.Empty;
            Enumerable.Range(48, 75)
                .Where(i => i < 58 || i > 64 && i < 91 || i > 96)
                .OrderBy(o => new Random().Next())
                .ToList()
                .ForEach(i => token += Convert.ToChar(i));
            token = token.Substring(new Random().Next(0, token.Length - 6), 6);
            return token;
        }
    }
}