using UrlShortening.Scripts.Data;

namespace UrlShortening.Scripts.Domain.Service
{
    public interface IEncodeService
    {
        UrlData EncodeUrl(string url);
    }
}