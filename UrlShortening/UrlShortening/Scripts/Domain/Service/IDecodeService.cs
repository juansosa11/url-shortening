using UrlShortening.Scripts.Domain;

namespace UrlShortening.Scripts.Controllers
{
    public interface IDecodeService
    {
        UrlData GetByShortUrl(string url);
        UrlData GetByToken(string token);
    }
}