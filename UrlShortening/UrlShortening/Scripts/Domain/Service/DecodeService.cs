using UrlShortening.Scripts.Controllers;
using UrlShortening.Scripts.Infrastructure.Repository;

namespace UrlShortening.Scripts.Domain.Service
{
    public class DecodeService : IDecodeService
    {
        private readonly IUrlsRepository _urlsRepository;
        
        public DecodeService(IUrlsRepository urlsUrlsRepository)
        {
            _urlsRepository = urlsUrlsRepository;
        }
        
        public UrlData GetByShortUrl(string shortUrl)
        {
            var urlData = _urlsRepository.TryGetValueByShortUrl(shortUrl);
            urlData?.OnRequest();
            return urlData;
        }

        public UrlData GetByToken(string token)
        {
            return _urlsRepository.TryGetValueByToken(token);
        }
    }
}