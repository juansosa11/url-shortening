using System;
using Microsoft.Extensions.Options;
using UrlShortening.Scripts.Data;
using UrlShortening.Scripts.Domain.Action;
using UrlShortening.Scripts.Infrastructure.Repository;

namespace UrlShortening.Scripts.Domain.Service
{
    public class EncodeService : IEncodeService
    {
        private readonly ITokenGenerator _tokenGenerator;
        private readonly IUrlsRepository _urlsRepository;
        private readonly ConfigurationSettings _configurationSettings;

        public EncodeService(ITokenGenerator tokenGenerator, IUrlsRepository urlsUrlsRepository, IOptions<ConfigurationSettings> configurationSettings)
        {
            _tokenGenerator = tokenGenerator;
            _urlsRepository = urlsUrlsRepository;
            _configurationSettings = configurationSettings.Value;
        }

        public UrlData EncodeUrl(string url)
        {
            url = NormalizeUrl(url);
            var urlData = _urlsRepository.TryGetValue(url);
            return urlData ?? GenerateUrl(url);
        }

        private UrlData GenerateUrl(string url)
        {
            var token = _tokenGenerator.Execute();
            var urlResponse = new UrlData(
                url,
                _configurationSettings.UrlBase + token,
                token,
                DateTime.Now,
                1,
                0
            );
            _urlsRepository.Store(urlResponse);
            return urlResponse;
        }
        
        private static string NormalizeUrl(string url) => url.Contains("http") ? url : "http://" + url;
    }
}