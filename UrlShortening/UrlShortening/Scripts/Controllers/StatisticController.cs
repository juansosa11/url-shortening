using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace UrlShortening.Scripts.Controllers
{
    [ApiController]
    [Route("statistic/{token}")]
    public class StatisticController : ControllerBase
    {
        private readonly IDecodeService _decodeService;
        private readonly ILogger<DecodeController> _logger;

        public StatisticController(IDecodeService decodeService, ILogger<DecodeController> logger)
        {
            _decodeService = decodeService;
            _logger = logger;
        }
        
        [HttpGet]
        public IActionResult Get(string token)
        {
            try
            {
                var response = _decodeService.GetByToken(token);
                
                if (response == null)
                    return NoContent();
                
                return Ok(JsonConvert.SerializeObject(response));
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error - StatisticController");
                return BadRequest(e);
            }
        }
    }
}