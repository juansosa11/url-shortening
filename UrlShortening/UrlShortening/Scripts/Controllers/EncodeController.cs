using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using UrlShortening.Scripts.Data;
using UrlShortening.Scripts.Domain.Service;

namespace UrlShortening.Scripts.Controllers
{
    [ApiController]
    [Route("encode")]
    public class EncodeController : ControllerBase
    {
        private readonly IEncodeService _encodeService;
        private readonly ILogger<EncodeController> _logger;

        public EncodeController(IEncodeService encodeService, ILogger<EncodeController> logger)
        {
            _encodeService = encodeService;
            _logger = logger;
        }
        
        [HttpGet]
        public IActionResult Get(string url)
        {
            try
            {
                var response = _encodeService.EncodeUrl(url);
                return Ok(new UrlResponse(response).ToJson());
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error - EncodeController");
                return BadRequest(e);
            }
        }
    }
}