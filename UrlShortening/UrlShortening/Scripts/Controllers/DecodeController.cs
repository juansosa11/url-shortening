using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using UrlShortening.Scripts.Data;

namespace UrlShortening.Scripts.Controllers
{
    [ApiController]
    [Route("decode")]
    public class DecodeController : ControllerBase
    {
        private readonly IDecodeService _decodeService;
        private readonly ILogger<DecodeController> _logger;
        private ConfigurationSettings _configurationSettings;

        public DecodeController(IDecodeService decodeService, ILogger<DecodeController> logger, IOptions<ConfigurationSettings> configurationSettings)
        {
            _decodeService = decodeService;
            _logger = logger;
            _configurationSettings = configurationSettings.Value;
        }
        
        [HttpGet]
        public IActionResult Get(string shortUrl)
        {
            try
            {
                if (!shortUrl.Contains(_configurationSettings.UrlBase))
                    return BadRequest("Invalid shortUrl");
                
                var response = _decodeService.GetByShortUrl(shortUrl);
                
                if (response == null)
                    return NoContent();

                return Ok(new UrlResponse(response).ToJson());
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error - DecodeController");
                return BadRequest(e);
            }
        }
    }
}