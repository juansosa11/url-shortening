using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using UrlShortening.Scripts.Controllers;
using UrlShortening.Scripts.Data;
using UrlShortening.Scripts.Domain;
using UrlShortening.Scripts.Domain.Action;
using UrlShortening.Scripts.Domain.Service;
using UrlShortening.Scripts.Infrastructure.Repository;

namespace UrlShortening.Scripts.Infrastructure
{
    public class Startup
    {
        private IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<IUrlsRepository, InMemoryUrlsRepository>();
            services.AddTransient<ITokenGenerator, TokenGenerator>();
            services.AddTransient<IEncodeService, EncodeService>();
            services.AddTransient<IDecodeService, DecodeService>();

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo {Title = "UrlShortening", Version = "v1"});
            });
            
            services.Configure<ConfigurationSettings>(Configuration.GetSection("ConfigurationSettings"));
        }
        
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "UrlShortening v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}