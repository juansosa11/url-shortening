using System.Collections.Generic;
using System.Linq;
using UrlShortening.Scripts.Domain;

namespace UrlShortening.Scripts.Infrastructure.Repository
{
    public class InMemoryUrlsRepository : IUrlsRepository
    {
        private static readonly Dictionary<string, UrlData> _cache = new();

        public UrlData TryGetValue(string url)
        {
            _cache.TryGetValue(url, out UrlData response);
            return response;
        }
        
        public void Store(UrlData response)
        {
            _cache.TryAdd(response.Url, response);
        }

        public UrlData TryGetValueByToken(object token)
        {
            if (!_cache.Any(url => url.Value.Token.Equals(token))) return null;
            var urlData = _cache.FirstOrDefault(url => url.Value.Token.Equals(token)).Value;
            urlData.UrlSubdomains = _cache.Count(url => url.Value.Url.Contains(urlData.Url));
            return urlData;
        }

        public UrlData TryGetValueByShortUrl(string shortUrl)
        {
            return _cache.FirstOrDefault(url => url.Value.ShortenedUrl.Equals(shortUrl)).Value;
        }
    }
}