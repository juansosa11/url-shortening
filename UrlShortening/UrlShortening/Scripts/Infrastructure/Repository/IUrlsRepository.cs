using UrlShortening.Scripts.Domain;

namespace UrlShortening.Scripts.Infrastructure.Repository
{
    public interface IUrlsRepository
    {
        UrlData TryGetValue(string url); 
        void Store(UrlData response);
        UrlData TryGetValueByShortUrl(string shortUrl);
        UrlData TryGetValueByToken(object shortUrl);
    }
}