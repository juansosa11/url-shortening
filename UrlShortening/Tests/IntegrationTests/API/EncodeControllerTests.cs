using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using NUnit.Framework;
using UrlShortening.Scripts.Data;

namespace Tests.IntegrationTests.API
{
    public class EncodeControllerTests
    {
        private ApiWebApplicationFactory _factory;
        private HttpClient _client;
        
        [SetUp]
        public void GivenARequestToTheController()
        {
            _factory = new ApiWebApplicationFactory();
            _client = _factory.CreateClient();
        }
        
        [Test]
        public async Task GivenAnUrlWhenInvokeEncodeThenTheResultIsOk()
        {
            const string shortUrl = "http://short.est/";
            var httpResponse = await _client.GetAsync("/encode?url=google.com");
            
            var result = httpResponse.Content.ReadAsStringAsync().Result;
            var urlResponse = JsonConvert.DeserializeObject<UrlResponse>(result);
            
            Assert.AreEqual(HttpStatusCode.OK, httpResponse.StatusCode);
            Assert.AreEqual("http://google.com", urlResponse.Url);
            Assert.IsTrue(urlResponse.ShortenedUrl.Contains(shortUrl));
        }
        
        [Test]
        public async Task GivenAnUrlWhenInvokeEncodeMultipleTimesThenTheResultIsEquals()
        {
            const string url = "google.com";
            var firstHttpResponse = await _client.GetAsync("/encode?url=" + url);
            var secondHttpResponse = await _client.GetAsync("/encode?url=" + url);
            
            var firstUrlResponse = JsonConvert.DeserializeObject<UrlResponse>(firstHttpResponse.Content.ReadAsStringAsync().Result);
            var secondUrlResponse = JsonConvert.DeserializeObject<UrlResponse>(secondHttpResponse.Content.ReadAsStringAsync().Result);
            
            Assert.AreEqual(HttpStatusCode.OK, firstHttpResponse.StatusCode);
            Assert.AreEqual(HttpStatusCode.OK, secondHttpResponse.StatusCode);

            Assert.AreEqual(firstUrlResponse.ShortenedUrl, secondUrlResponse.ShortenedUrl);
        }
    }
}