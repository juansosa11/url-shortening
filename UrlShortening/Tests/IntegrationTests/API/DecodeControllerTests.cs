using System.Net;
using System.Net.Http;
using System.Security.Policy;
using System.Threading.Tasks;
using Newtonsoft.Json;
using NUnit.Framework;
using UrlShortening.Scripts.Data;

namespace Tests.IntegrationTests.API
{
    public class DecodeControllerTests
    {
        private ApiWebApplicationFactory _factory;
        private HttpClient _client;
        
        [SetUp]
        public void GivenARequestToTheController()
        {
            _factory = new ApiWebApplicationFactory();
            _client = _factory.CreateClient();
        }
        
        [Test]
        public async Task GivenAnNotShortenedUrlWhenInvokeDecodeThenTheResultIsNoContent()
        {
            var decodeHttpResponse = await _client.GetAsync("/decode?shortUrl=http://short.est/test1");
            Assert.AreEqual(HttpStatusCode.NoContent, decodeHttpResponse.StatusCode);
        }
        
        [Test]
        public async Task GivenAnInvalidUrlWhenInvokeDecodeThenTheResultIsBadRequest()
        {
            var decodeHttpResponse = await _client.GetAsync("/decode?shortUrl=invalidUrl");
            Assert.AreEqual(HttpStatusCode.BadRequest, decodeHttpResponse.StatusCode);
        }
        
        [Test]
        public async Task GivenAnUrlWhenInvokeEncodeAndDecodeThenTheResultIsOk()
        {
            const string url = "google.com";
            var encodeHttpResponse = await _client.GetAsync("/encode?url=" + url);
            var encodeUrlResponse = JsonConvert.DeserializeObject<UrlResponse>(encodeHttpResponse.Content.ReadAsStringAsync().Result);
            
            var decodeHttpResponse = await _client.GetAsync("/decode?shortUrl=" + encodeUrlResponse.ShortenedUrl);
            var decodeUrlResponse = JsonConvert.DeserializeObject<UrlResponse>(decodeHttpResponse.Content.ReadAsStringAsync().Result);
            
            Assert.AreEqual(HttpStatusCode.OK, decodeHttpResponse.StatusCode);
            Assert.AreEqual(encodeUrlResponse.Url, decodeUrlResponse.Url);
            Assert.AreEqual(encodeUrlResponse.ShortenedUrl, decodeUrlResponse.ShortenedUrl);
        }
    }
}