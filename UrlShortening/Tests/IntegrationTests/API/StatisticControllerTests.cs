using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using NUnit.Framework;
using UrlShortening.Scripts.Data;
using UrlShortening.Scripts.Domain;

namespace Tests.IntegrationTests.API
{
    public class StatisticControllerTests
    {
        private ApiWebApplicationFactory _factory;
        private HttpClient _client;
        
        [SetUp]
        public void GivenARequestToTheController()
        {
            _factory = new ApiWebApplicationFactory();
            _client = _factory.CreateClient();
        }
        
        [Test]
        public async Task GivenAnUrlWithMultileSubDomainsWhenInvokeStatisticThenTheResultIsOk()
        {
            var encodeHttpResponse = await _client.GetAsync("/encode?url=http://google.com");
            await _client.GetAsync("/encode?url=http://google.com/subdomain1");
            await _client.GetAsync("/encode?url=http://google.com/subdomain2");

            var encodeResult = encodeHttpResponse.Content.ReadAsStringAsync().Result;
            var encodeUrlResponse = JsonConvert.DeserializeObject<UrlResponse>(encodeResult);
            
            var decodeHttpResponse = await _client.GetAsync("/decode?shortUrl=" + encodeUrlResponse.ShortenedUrl);
            var decodeUrlResponse = JsonConvert.DeserializeObject<UrlResponse>(decodeHttpResponse.Content.ReadAsStringAsync().Result);
            var token = decodeUrlResponse.ShortenedUrl.Substring(decodeUrlResponse.ShortenedUrl.Length - 6, 6);
            
            var statisticHttpResponse = await _client.GetAsync("/statistic/" + token);
            var statisticUrlResponse = JsonConvert.DeserializeObject<UrlData>(statisticHttpResponse.Content.ReadAsStringAsync().Result);
            
            Assert.AreEqual(HttpStatusCode.OK, statisticHttpResponse.StatusCode);
            Assert.AreEqual(encodeUrlResponse.Url, statisticUrlResponse.Url);
            Assert.AreEqual(encodeUrlResponse.ShortenedUrl, statisticUrlResponse.ShortenedUrl);
            Assert.AreEqual(3, statisticUrlResponse.UrlSubdomains);
        }
    }
}