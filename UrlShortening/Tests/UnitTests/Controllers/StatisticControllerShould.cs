using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NSubstitute;
using NSubstitute.ReturnsExtensions;
using NUnit.Framework;
using UrlShortening.Scripts.Controllers;
using UrlShortening.Scripts.Domain;

namespace Tests.UnitTests.Controllers
{
    public class StatisticControllerShould
    {
        private IDecodeService _decodeService;
        private StatisticController _statisticController;
            
        [SetUp]
        public void Setup()
        {
            _decodeService = Substitute.For<IDecodeService>();
            var logger = Substitute.For<ILogger<DecodeController>>();

            _statisticController = new StatisticController(_decodeService, logger);
        }
        
        [Test]
        public void GivenAValidTokenWhenInvokeServiceThenResponseStatusIsOk()
        {
            var token = "token1";
            
            var urlExpected = UrlMother.AnUrlData(
                withUrl: "https://google.com", 
                withShortenedUrl: "http://short.est/" + token,
                withToken: token);
            
            GivenAnTokenWhenInvokeService(urlExpected, token);
            
            var response = WhenGet(token);
            
            ThenResponseStatusOk(response);
        }
        
        [Test]
        public void GivenAnInvalidTokenWhenInvokeServiceThenResponseNull()
        {
            var token = "invalid";
            GivenAnInvalidTokenWhenInvokeService(token);
            
            var response = WhenGet(token);
            
            ThenResponseIsNoContent(response);
        }
        
        private void GivenAnTokenWhenInvokeService(UrlData urlExpected, string token)
        {
            _decodeService.GetByToken(token).Returns(urlExpected);
        }
        
        private void GivenAnInvalidTokenWhenInvokeService(string token)
        {
            _decodeService.GetByToken(token).ReturnsNull();
        }
        
        private IActionResult WhenGet(string token)
        {
            return _statisticController.Get(token);
        }
        
        private static void ThenResponseStatusOk(IActionResult response)
        {
            var okObjectResult = response as OkObjectResult;
            Assert.IsNotNull(okObjectResult);
        }
        
        private void ThenResponseIsNoContent(IActionResult response)
        {
            var badObjectResult = response as NoContentResult;
            Assert.IsNotNull(badObjectResult);
        }
    }
}