using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using NSubstitute.ReturnsExtensions;
using NUnit.Framework;
using UrlShortening.Scripts.Controllers;
using UrlShortening.Scripts.Data;
using UrlShortening.Scripts.Domain;

namespace Tests.UnitTests.Controllers
{
    public class DecodeControllerShould
    {
        private IDecodeService _decodeService;
        private DecodeController _decodeController;
        private const string _url = "http://google.com";
        private const string _shortUrl = "http://short.est/token1";
        
        [SetUp]
        public void Setup()
        {
            _decodeService = Substitute.For<IDecodeService>();
            var logger = Substitute.For<ILogger<DecodeController>>();

            var configurationSettings = Substitute.For<IOptions<ConfigurationSettings>>();
            configurationSettings.Value.Returns(new ConfigurationSettings() {UrlBase = "http://short.est/"});
            
            _decodeController = new DecodeController(_decodeService, logger, configurationSettings);
        }
        
        [Test]
        public void GivenAValidShortUrlWhenInvokeServiceThenResponseStatusIsOk()
        {
            var urlExpected = UrlMother.AnUrlData(withUrl: _url, withShortenedUrl: _shortUrl);
            GivenAnUrlWhenInvokeService(urlExpected);
            
            var response = WhenGet();
            
            ThenResponseStatusOk(response);
        }
        
        [Test]
        public void GivenAValidShortUrlWhenInvokeServiceThenValueIsCorrect()
        {
            var urlExpected = UrlMother.AnUrlData(withUrl: _url, withShortenedUrl: _shortUrl);
            GivenAnUrlWhenInvokeService(urlExpected);
            
            var response = WhenGet();
            
            ThenResponseContainsShortUrl(response, urlExpected);
        }
        
        [Test]
        public void GivenABadRequestWhenInvokeService()
        {
            GivenAThrowExceptionWhenInvokeService();
            
            var response = WhenGet();
            
            ThenResponseIsBadRequest(response);
        }
        
        [Test]
        public void GivenAnInvalidShortUrWhenInvokeServiceThenReceiveBadRequest()
        {
            GivenAnAnInvalidShortUrWhenInvokeService("invalidShortUrl");
            
            var response = WhenGet("invalidShortUrl");
            
            ThenResponseIsBadRequest(response);
        }
        
        [Test]
        public void GivenAnNotShortenedUrWhenInvokeServiceThenReceiveNoContent()
        {
            var shortUrl = "http://short.est/test1";
            GivenAnAnInvalidShortUrWhenInvokeService(shortUrl);
            
            var response = WhenGet(shortUrl);
            
            ThenResponseIsNoContent(response);
        }

        private void GivenAnAnInvalidShortUrWhenInvokeService(string invalidShortUrl)
        {
            _decodeService.GetByShortUrl(invalidShortUrl).ReturnsNull();
        }

        private void GivenAThrowExceptionWhenInvokeService()
        {
            _decodeService.GetByShortUrl(_shortUrl).Throws(new Exception());
        }

        private void GivenAnUrlWhenInvokeService(UrlData urlExpected)
        {
            _decodeService.GetByShortUrl(_shortUrl).Returns(urlExpected);
        }

        private IActionResult WhenGet(string url = _shortUrl)
        {
            return _decodeController.Get(url);
        }

        private void ThenResponseIsBadRequest(IActionResult response)
        {
            var badObjectResult = response as BadRequestObjectResult;
            Assert.IsNotNull(badObjectResult);
        }

        private void ThenResponseIsNoContent(IActionResult response)
        {
            var badObjectResult = response as NoContentResult;
            Assert.IsNotNull(badObjectResult);
        }

        private static void ThenResponseStatusOk(IActionResult response)
        {
            var okObjectResult = response as OkObjectResult;
            Assert.IsNotNull(okObjectResult);
        }

        private static void ThenResponseContainsShortUrl(IActionResult response, UrlData urlExpected)
        {
            var okObjectResult = response as OkObjectResult;
            var result = (string) okObjectResult?.Value;

            Assert.IsTrue(result.Contains(urlExpected.ShortenedUrl));
        }
    }
}