using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using NUnit.Framework;
using UrlShortening.Scripts.Controllers;
using UrlShortening.Scripts.Domain;
using UrlShortening.Scripts.Domain.Service;

namespace Tests.UnitTests.Controllers
{
    public class EncodeControllerShould
    {
        private IEncodeService _encodeService;
        private EncodeController _encodeController;
        private const string _url = "https://google.com";
        
        [SetUp]
        public void Setup()
        {
            _encodeService = Substitute.For<IEncodeService>();
            var logger = Substitute.For<ILogger<EncodeController>>();
            _encodeController = new EncodeController(_encodeService, logger);
        }
        
        [Test]
        public void GivenAValidUrlWhenInvokeServiceThenResponseStatusIsOk()
        {
            var urlExpected = UrlMother.AnUrlData(withUrl: _url, withShortenedUrl: "http://short.est/token1");
            GivenAnEncodeService(urlExpected);
            
            var response = WhenCallGetController();
            
            ThenResponseStatusOk(response);
        }
        
        [Test]
        public void GivenAValidUrlWhenInvokeServiceThenValueIsCorrect()
        {
            var urlExpected = UrlMother.AnUrlData(withUrl: _url, withShortenedUrl: "http://short.est/token1");
            GivenAnEncodeService(urlExpected);
            
            var response = WhenCallGetController();
            
            ThenResponseContainsShortUrl(response, urlExpected);
        }
        
        [Test]
        public void ReceiveBadRequestWhenInvokeService()
        {
            GivenThrowExceptionEncodeService();
            
            var response = WhenCallGetController();
            
            ThenResponseIsBadRequest(response);
        }

        private void GivenThrowExceptionEncodeService()
        {
            _encodeService.EncodeUrl(Arg.Any<string>()).Throws(new Exception());
        }

        private void GivenAnEncodeService(UrlData urlExpected)
        {
            _encodeService.EncodeUrl(_url).Returns(urlExpected);
        }

        private IActionResult WhenCallGetController()
        {
            return _encodeController.Get(_url);
        }

        private void ThenResponseIsBadRequest(IActionResult response)
        {
            var badObjectResult = response as BadRequestObjectResult;
            Assert.IsNotNull(badObjectResult);
        }

        private static void ThenResponseStatusOk(IActionResult response)
        {
            var okObjectResult = response as OkObjectResult;
            Assert.IsNotNull(okObjectResult);
        }
        
        private static void ThenResponseContainsShortUrl(IActionResult response, UrlData urlExpected)
        {
            var okObjectResult = response as OkObjectResult;
            var result = (string) okObjectResult?.Value;

            Assert.IsTrue(result.Contains(urlExpected.ShortenedUrl));
        }
    }
}