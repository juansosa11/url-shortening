using System;
using UrlShortening.Scripts.Domain;

namespace Tests.UnitTests
{
    public static class UrlMother
    {
        public static UrlData AnUrlData(
            string withUrl = "", 
            string withShortenedUrl = "",
            string withToken = "",
            DateTime withCreatedDate = default,
            int withRequested = 0,
            int withUrlSubdomains = 0)
            => new UrlData(withUrl, withShortenedUrl, withToken, withCreatedDate, withRequested, withUrlSubdomains);
    }
}