using Microsoft.Extensions.Options;
using NSubstitute;
using NSubstitute.ReturnsExtensions;
using NUnit.Framework;
using UrlShortening.Scripts.Data;
using UrlShortening.Scripts.Domain;
using UrlShortening.Scripts.Domain.Action;
using UrlShortening.Scripts.Domain.Service;
using UrlShortening.Scripts.Infrastructure.Repository;

namespace Tests.UnitTests.Services
{
    public class EncodeServiceShould
    {
        private EncodeService _encodeService;
        private ITokenGenerator _tokenGenerator;
        private IUrlsRepository _urlsRepository;
        private IOptions<ConfigurationSettings> _configurationSettings;
        private string _url = "http://google.com";
        private const string _token = "token1";
        private const string _urlBase = "http://short.est/";

        [SetUp]
        public void Setup()
        {
            _tokenGenerator = Substitute.For<ITokenGenerator>();
            _tokenGenerator.Execute().Returns(_token);
            
            _urlsRepository = Substitute.For<IUrlsRepository>();
            _configurationSettings = Substitute.For<IOptions<ConfigurationSettings>>();
            _configurationSettings.Value.Returns(new ConfigurationSettings() {UrlBase = _urlBase});
            _encodeService = new EncodeService(_tokenGenerator, _urlsRepository, _configurationSettings);
        }
        
        [Test]
        public void GivenAnUrlCorrectResultWhenEncodeUrl()
        {
            GivenARepositoryWithNotUrl(_url);
            
            var result = WhenEncodeUrl(_url);
            ThenUrlIsCorrect(result, _url);
            ThenShortUrlIsCorrect(result);
            ThenShortUrlHasCorrectLenght(result);
        }

        [Test]
        public void NormalizeUrlWhenEncodeUrl()
        {
            GivenARepositoryWithNotUrl(_url);
            
            var result = WhenEncodeUrl("google.com");
            
            ThenUrlIsCorrect(result, _url);
            ThenShortUrlIsCorrect(result);
            ThenShortUrlHasCorrectLenght(result);
        }

        [Test]
        public void DoNotStoreDuplicateUrlWhenEncodeUrl()
        {
            var urlExpected = UrlMother.AnUrlData(
                withUrl: _url,
                withShortenedUrl: _urlBase + _token);
            GivenARepositoryWithAnUrl(urlExpected);

            WhenEncodeUrl(_url);
            
            ThenDidNotStoreDuplicateUrlInRepository();
        }

        private void GivenARepositoryWithAnUrl(UrlData urlExpected)
        {
            _urlsRepository.TryGetValue(_url).Returns(urlExpected);
        }

        private void GivenARepositoryWithNotUrl(string url)
        {
            _urlsRepository.TryGetValue(url).ReturnsNull();
        }

        private UrlData WhenEncodeUrl(string url)
        {
            return _encodeService.EncodeUrl(url);
        }

        private void ThenDidNotStoreDuplicateUrlInRepository()
        {
            _urlsRepository.DidNotReceive().Store(Arg.Any<UrlData>());
        }

        private void ThenShortUrlIsCorrect(UrlData result)
        {
            Assert.IsTrue(result.ShortenedUrl.Equals(_urlBase+_token));
        }

        private static void ThenShortUrlHasCorrectLenght(UrlData result)
        {
            Assert.IsTrue(result.ShortenedUrl.Length == 23);
        }

        private static void ThenUrlIsCorrect(UrlData result, string url)
        {
            Assert.IsTrue(result.Url.Equals(url));
        }
    }
}