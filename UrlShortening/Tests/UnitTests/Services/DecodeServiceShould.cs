using NSubstitute;
using NSubstitute.ReturnsExtensions;
using NUnit.Framework;
using UrlShortening.Scripts.Domain;
using UrlShortening.Scripts.Domain.Service;
using UrlShortening.Scripts.Infrastructure.Repository;

namespace Tests.UnitTests.Services
{
    public class DecodeServiceShould
    {
        private DecodeService _decodeService;
        private IUrlsRepository _urlsRepository;
        private const string _shortUrl = "http://short.est/" + _token;
        private const string _token = "token1";
        private const string _url = "https://google.com";
        
        [SetUp]
        public void Setup()
        {
            _urlsRepository = Substitute.For<IUrlsRepository>();
            _decodeService = new DecodeService(_urlsRepository);
        }
        
        [Test]
        public void GivenAnShortUrlValidWhenEncodeUrlThenResponseStatusIsOk()
        {
            var urlExpected = UrlMother.AnUrlData(withUrl: _url, withShortenedUrl: _shortUrl);
            GivenARepositoryWithAnUrl(urlExpected);
           
            var result = WhenTryGetValueByShortUrl();
            
            ThenUrlIsCorrect(result);
        }
        
        [Test]
        public void GivenAnShortUrlValidWhenEncodeUrlThenOneAddedToRequested()
        {
            var urlExpected = UrlMother.AnUrlData(withUrl: _url, withShortenedUrl: _shortUrl, withRequested: 1);
            GivenARepositoryWithAnUrl(urlExpected);
           
            var result = WhenTryGetValueByShortUrl();
            
            ThenUrlIsRequested(result);
        }

        [Test]
        public void GivenAnInvalidShortUrlWhenEncodeUrlThenReturnNull()
        {
            GivenARepositoryWithAnInvalidShortUrl();
           
            var result = WhenTryGetValueByShortUrl();
            
            ThenUrlIsNull(result);
        }
        
        [Test]
        public void GivenATokenValidWhenDecodeUrlThenResponseStatusIsOk()
        {
            var urlExpected = UrlMother.AnUrlData(withUrl: _url, withShortenedUrl: _shortUrl, withToken: _token);
            GivenARepositoryWithAToken(urlExpected);
           
            var result = WhenTryGetValueByByToken();
            
            ThenUrlIsCorrect(result);
        }
        
        [Test]
        public void GivenAnInvalidTokenValidWhenDecodeUrlThenResponseStatusIsBadRequest()
        {
            var urlExpected = UrlMother.AnUrlData(withToken: "invalid");
            GivenARepositoryWithAnInvalidToken(urlExpected);
           
            var result = WhenTryGetValueByByToken();
            
            ThenUrlIsNull(result);
        }

        private void GivenARepositoryWithAnInvalidShortUrl()
        {
            _urlsRepository.TryGetValueByShortUrl(_shortUrl).ReturnsNull();
        }

        private void GivenARepositoryWithAToken(UrlData urlExpected)
        {
            _urlsRepository.TryGetValueByToken(_token).Returns(urlExpected);
        }
        
        private void GivenARepositoryWithAnInvalidToken(UrlData urlExpected)
        {
            _urlsRepository.TryGetValueByToken(_token).ReturnsNull();
        }
        
        private void GivenARepositoryWithAnUrl(UrlData urlExpected)
        {
            _urlsRepository.TryGetValueByShortUrl(_shortUrl).Returns(urlExpected);
        }

        private UrlData WhenTryGetValueByShortUrl()
        {
            return _decodeService.GetByShortUrl(_shortUrl);
        }

        private UrlData WhenTryGetValueByByToken()
        {
            return _decodeService.GetByToken(_token);
        }

        private void ThenUrlIsNull(UrlData result)
        {
            Assert.IsNull(result);
        }

        private void ThenUrlIsCorrect(UrlData result)
        {
            Assert.IsTrue(result.Url.Equals(_url));
            Assert.IsTrue(result.ShortenedUrl.Equals(_shortUrl));
        }

        private void ThenUrlIsRequested(UrlData result)
        {
            Assert.IsTrue(result.Requested == 2);
        }
    }
}