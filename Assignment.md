# url-shortening

## Objective

Your assignment is to implement a URL shortening service using your programming language of
choice (NodeJS is preferred).


## Brief

ShortLink is a URL shortening service where you enter a URL such as https://indicina.co and it
returns a short URL such as http://short.est/GeAi9K. Visiting the shortened URL should redirect
the user to the long URL. Using the example above, visiting http://short.est/GeAi9K should
redirect the user to https://indicina.co.


## Tasks

Implement using:

- Language: Any but preferably NodeJS
- Framework: Any framework or none
Three endpoints are required:
- `/encode` - Encodes a URL to a shortened URL
- `/decode` - Decodes a shortened URL to its original URL
- `/statistic/{url_path}` - Return basic stat of a short URL path. Using the above link
url_path will be GeAi9K. (Be creative with what details you provide here.)

## Additionally:

- All endpoints should return a JSON response
- There is no restriction to how your encode/decode algorithm should work. You just need
to make sure that a URL can be encoded to a short URL and the short URL can be
decoded to the original URL. Keep them in memory
- Provide detailed instructions on how to run your assignment in a separate markdown file
- Provide API tests for both endpoints
- On every step of the project, commit the work done. We will check the git log and check
how you code
- Duration of this test is 1 week. To submit, send a zip file containing the code and the git
folder to join@indicina.co
- Provide detailed instructions on how to run the code and test.


## Evaluation Criteria

- Best practices
- SOLID principles
- Clean Code.
- Tests! Tests!! Tests!!!