# url-shortening

# Objective

Your assignment is to implement a URL shortening service using your programming language of
choice (NodeJS is preferred).

# Intent
This software is an url shortener, it stores a given original url and sends back a tockenized short url for ease of sharing and control over its spread.

# Documentation

## Usage

- Clone the repository
- Build solution
- Run Application

### End Points

#### `/encode` - Encodes a URL to a shortened URL

Example:
- Request: encode?url=google.com
- Server response:
```json
{"Url":"http://google.com","ShortenedUrl":"http://short.est/token1"}
```

#### `/decode` - Decodes a shortened URL to its original URL

Example:
- Request: decode?url=http://short.est/token1
- Server response:
```json
{"Url":"http://google.com","ShortenedUrl":"http://short.est/token1"}
```

#### `/statistic/{url_path}` - Return basic stat of a short URL path. Using the above link url_path will be token1

Example:
- Request: statistic/token1
- Server response:


```json
{"Url":"http://google.com","ShortenedUrl":"http://short.est/NKqsuf","Token":"NKqsuf","CreatedDate":"2021-07-30T12:42:06.86929-03:00","Requested":1,"UrlSubdomains":1}
```

## TESTS

- For run click right in Test Solution and click in "Run Unit Tests"
- For execute an specific UT, go to file, click in test and click "Run"

### Integration Tests (Tests for API)

One file for each EP

- DecodeControllerTests
- EncodeControllerTests
- StatisticControllerTests 

## Unit Tests (Tests for controllers and services)

- DecodeController
- EncodeController
- StatisticController
- DecodeService
- EncodeService

## PENDING

- Add tests for TokenGenerator.
- Ensure the token is unique.
- Improve error logging.
- Serialization tag in classes and property, this will allow removing the public attribute to the properties.